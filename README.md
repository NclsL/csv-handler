Asynchronous CSV upload 

To run, server and front need to be started separately with `yarn && yarn start` in respective folders, though since it's not currently dockerized it also needs mongo service running

For code which handles the async process:

(Front > src > Filedropzone.js rows 19-40)

(Back > server > server.js rows 19-40)

CSV parsing is done with library that utilizes promises inside async block. While it may take a short time to finish, it is not blocking.

The database query will find the user requested FIELD and return the TOP N rows where the FIELD has the largest value.

![](./preview0.png)