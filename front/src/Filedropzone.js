import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import {
	 Icon,
	 Segment,
	 Button,
	 Header
       } from 'semantic-ui-react';
import Dropzone from 'react-dropzone';

export default class Filedropzone extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadStatus: 'Not uploading'
    };
  }

  async upload(file) {
    const formData = new FormData();
    formData.append('csv', file);

    const opts = {
      method: 'POST',
      mode: 'cors',
      body: formData
    };

    try {
      this.setState({uploadStatus: 'Uploading...'});
      const res = await fetch('http://localhost:8001/upload-csv', opts)
        .then((response) => {
            if(!response.ok) {
                this.setState({uploadStatus: 'Upload failed'});
                throw new Error(response.status);
            }
            else return response.json();
        })
	    .then(this.setState({uploadStatus: 'Upload Successful'}))
            .then(setTimeout(() => {
              this.setState({uploadStatus: 'Not uploading'});
            }, 3000));
    } catch (e) {
      this.setState({uploadStatus: 'Upload failed'});
    }
  }

  render() {
    return(
      <Dropzone onDrop={ acceptedFiles => this.upload(acceptedFiles[0]) } accept="text/csv">
	{({getRootProps, getInputProps, isDragActive, acceptedFiles}) => {
	  return (
	    <div {...getRootProps()} >
	      <input {...getInputProps()} />
	      <Segment placeholder>
		{
		  isDragActive ?
		    <Header>
		      Drop here
		    </Header>
		    :
		    <>
		      <Header icon>
                        <p>Current status: {this.state.uploadStatus}</p>
			<Icon name="file alternate"/>
			Drag and drop
			<br/>
			Or
		      </Header>
		      <Button type="button">
			<Icon name="upload"/>
			Upload
		      </Button>
		    </>
		}
	      </Segment>
	    </div>
	  );
	}}
      </Dropzone>
    );
  }
}
