import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import { Grid,
         Button,
         Segment,
         Input
       } from 'semantic-ui-react';
import Filedropzone from './Filedropzone';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recordsToFetch: '',
      fieldName: '',
      records: []
    };
  }
  
  async fetchRecords() {
    const URL = `http://localhost:8001/fetch-top?fieldName=${this.state.fieldName}&top=${this.state.recordsToFetch}`;
    try {
      const res = await fetch(URL)
	    .then(response => response.json())
	    .then(json => this.setState({records: json}));
    } catch (e) {
      console.log(e);
    }
  }
  
  render() {
    return (
      <div>
        <Grid divided columns={2}>
          <Grid.Column textAlign="center" style={{paddingLeft: "2rem"}}>
            <Filedropzone style={{paddingTop: "4rem"}}/>
          </Grid.Column>
          <Grid.Column textAlign="center" style={{paddingTop: "2rem", paddingRight: "2rem"}}>
            <Input type="number" placeholder="Nr of top records" value={this.state.recordsToFetch} onChange={(e) => this.setState({recordsToFetch: e.target.value})}/>
            <Input placeholder="Field name e.g. FIELD" value={this.state.fieldName} onChange={(e) => this.setState({fieldName: e.target.value})}/>
            <Button onClick={() => this.fetchRecords()}basic color="green" style={{paddingTop: "12px", paddingBottom: "12px", marginLeft: "10px"}}>Fetch</Button>
            <Segment>
              {this.state.records.map(record => (
                <p>{record._id} ; {record.FIELD} ; {record.TEST} ; {record.COLUMN}</p>
              ))
              }
            </Segment>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default App;
