const cors = require('cors');
const app = require('express')();
const formData = require('express-form-data');
const client = require('mongodb').MongoClient;
const csvtojson = require('csvtojson');

let dbPromise = null;

const getDb = () => dbPromise || (dbPromise = client.connect("mongodb://localhost:27017/CSV-handler", { useNewUrlParser: true }).then((cl) => cl.db()));

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(formData.parse());

app.post('/upload-csv', async (req, res) => {
  const csvs = (await getDb()).collection('csvs');
  csvtojson().fromFile(req.files.csv.path)
    .then((jsonObj) => {
      jsonObj.map(async (obj) => {
        await csvs.insertOne(obj);
      });
    });
});

app.get('/fetch-top', async (req, res) => {
  const amountOfRecords = parseInt(req.query.top);
  const fieldName = req.query.fieldName;
  
  const csvs = (await getDb()).collection('csvs');
  const result = await csvs.find().sort({[fieldName]: -1}).limit(amountOfRecords).toArray((error, result) => {
    if (error) {
      console.log(error);
    } 
    res.send(result);
  });
});


app.listen(8001, () => console.log('listening on 8001'));
